import numpy as np
import xgboost as xgb

from wvz_helpers import load_dataframes, load_dataframe, skims
import pandas as pd

from train_bdt import zz_features, ttz_features

import os

models = {
    "EMu_ttz_score_wwz": "models/wwz/emu_ttz.bin",
    "EMu_zz_score_wwz": "models/wwz/emu_zz.bin",
    "OffZ_zz_score_wwz": "models/wwz/offz_zz.bin",
    "EMu_ttz_score_nonh_wwz": "models/nonh_wwz/emu_ttz.bin",
    "EMu_zz_score_nonh_wwz": "models/nonh_wwz/emu_zz.bin",
    "OffZ_zz_score_nonh_wwz": "models/nonh_wwz/offz_zz.bin",
}

for sample in skims.keys():
    print("   processing " + sample + "...")
    try:
        df = load_dataframe(sample)
    except:
        print("sample " + sample + " could not be evaluated")
        continue

    dmatrix_ttz = xgb.DMatrix(df[ttz_features])
    dmatrix_zz = xgb.DMatrix(df[zz_features])

    for score_column, model_file in models.items():

        print("Evaluating " + score_column + "...")

        bst = xgb.Booster({"nthread": 4})
        bst.load_model(model_file)

        score_path = os.path.dirname(model_file.replace("models", "scores"))
        score_file = model_file.replace("models", "scores").replace(".bin", f"_sample_{sample}.hdf")

        if not os.path.exists(score_path):
            os.makedirs(score_path)

        if "ttz" in model_file:
            scores = bst.predict(dmatrix_ttz)
        elif "zz" in model_file:
            scores = bst.predict(dmatrix_zz)
        else:
            raise NotImplementedError()

        pd.DataFrame({score_column: scores}).to_hdf(score_file, key="scores")
