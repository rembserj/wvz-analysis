import uproot
import os
import glob
import numpy as np
import pandas as pd
import uproot_methods
import geeksw.plotting.cmsplot as plt
from matplotlib import gridspec

from wvz_helpers import load_dataframes, load_dataframe, yields_map, skims
from syncronization import load_philips_yields
import wvz_plotting

import scipy.stats

df = load_dataframes(["wwz", "ttz", "zz", "nonh_wwz", "zh_wwz"], concat=True)

df_offz_train = df.query("is_ChannelOffZ and used_for_OffZ_training")
df_offz_test = df.query("is_ChannelOffZ and not used_for_OffZ_training")

df_emu_train = df.query("is_ChannelEMu and used_for_EMu_training")
df_emu_test = df.query("is_ChannelEMu and not used_for_EMu_training")

hist_args = dict(histtype="step", density=True, lw=2)


def hist(df, col, **kwargs):
    plt.hist(df[col], weights=None, **kwargs, **hist_args)


bins = np.linspace(-8, 12, 100)

print("Doing OffZ WWZ vs ZZ BDT score...")
print("KS test for WWZ:")
print(
    scipy.stats.ks_2samp(
        df_offz_test.query('sample == "wwz"')["bdt_OffZ_wwz_vs_zz"],
        df_offz_train.query('sample == "wwz"')["bdt_OffZ_wwz_vs_zz"],
    )
)
print("KS test for ZZ:")
print(
    scipy.stats.ks_2samp(
        df_offz_test.query('sample == "zz"')["bdt_OffZ_wwz_vs_zz"],
        df_offz_train.query('sample == "zz"')["bdt_OffZ_wwz_vs_zz"],
    )
)

plt.figure()
plt.yscale("log", nonposy="clip")
hist(df_offz_test.query('sample == "wwz"'), "bdt_OffZ_wwz_vs_zz", label="WWZ test", bins=bins)
hist(df_offz_train.query('sample == "wwz"'), "bdt_OffZ_wwz_vs_zz", label="WWZ train", bins=bins)
hist(df_offz_test.query('sample == "zz"'), "bdt_OffZ_wwz_vs_zz", label="ZZ test", bins=bins)
hist(df_offz_train.query('sample == "zz"'), "bdt_OffZ_wwz_vs_zz", label="ZZ train", bins=bins)
plt.legend(loc="upper right", ncol=2)
plt.ylim(plt.ylim()[0], 10 * plt.ylim()[1])
plt.xlabel("OffZ WWZ vs ZZ BDT score")
plt.savefig("plots/bdt_scores/bdt_OffZ_wwz_vs_zz.pdf")
plt.savefig("plots/bdt_scores/bdt_OffZ_wwz_vs_zz.png", dpi=300)

bins = np.linspace(-7, 12, 100)

print("Doing EMu WWZ vs ZZ BDT score...")
print("KS test for WWZ:")
print(
    scipy.stats.ks_2samp(
        df_emu_test.query('sample == "wwz"')["bdt_EMu_wwz_vs_zz"],
        df_emu_train.query('sample == "wwz"')["bdt_EMu_wwz_vs_zz"],
    )
)
print("KS test for ZZ:")
print(
    scipy.stats.ks_2samp(
        df_emu_test.query('sample == "zz"')["bdt_EMu_wwz_vs_zz"],
        df_emu_train.query('sample == "zz"')["bdt_EMu_wwz_vs_zz"],
    )
)


plt.figure()
plt.yscale("log", nonposy="clip")
hist(df_emu_test.query('sample == "wwz"'), "bdt_EMu_wwz_vs_zz", label="WWZ test", bins=bins)
hist(df_emu_train.query('sample == "wwz"'), "bdt_EMu_wwz_vs_zz", label="WWZ train", bins=bins)
hist(df_emu_test.query('sample == "zz"'), "bdt_EMu_wwz_vs_zz", label="ZZ test", bins=bins)
hist(df_emu_train.query('sample == "zz"'), "bdt_EMu_wwz_vs_zz", label="ZZ train", bins=bins)
plt.legend(loc="upper right", ncol=2)
plt.ylim(plt.ylim()[0], 10 * plt.ylim()[1])
plt.xlabel("EMu WWZ vs ZZ BDT score")
plt.savefig("plots/bdt_scores/bdt_EMu_wwz_vs_zz.pdf")
plt.savefig("plots/bdt_scores/bdt_EMu_wwz_vs_zz.png", dpi=300)

bins = np.linspace(-5, 10, 100)

print("Doing EMu WWZ vs ttZ BDT score...")
print("KS test for WWZ:")
print(
    scipy.stats.ks_2samp(
        df_emu_test.query('sample == "wwz"')["bdt_EMu_wwz_vs_ttz"],
        df_emu_train.query('sample == "wwz"')["bdt_EMu_wwz_vs_ttz"],
    )
)
print("KS test for ttZ:")
print(
    scipy.stats.ks_2samp(
        df_emu_test.query('sample == "ttz"')["bdt_EMu_wwz_vs_ttz"],
        df_emu_train.query('sample == "ttz"')["bdt_EMu_wwz_vs_ttz"],
    )
)


plt.figure()
plt.yscale("log", nonposy="clip")
hist(df_emu_test.query('sample == "wwz"'), "bdt_EMu_wwz_vs_ttz", label="WWZ test", bins=bins)
hist(df_emu_train.query('sample == "wwz"'), "bdt_EMu_wwz_vs_ttz", label="WWZ train", bins=bins)
hist(df_emu_test.query('sample == "ttz"'), "bdt_EMu_wwz_vs_ttz", label="ttZ test", bins=bins)
hist(df_emu_train.query('sample == "ttz"'), "bdt_EMu_wwz_vs_ttz", label="ttZ train", bins=bins)
plt.legend(loc="upper right", ncol=2)
plt.ylim(plt.ylim()[0], 10 * plt.ylim()[1])
plt.xlabel("EMu WWZ vs ttZ BDT score")
plt.savefig("plots/bdt_scores/bdt_EMu_wwz_vs_ttz.pdf")
plt.savefig("plots/bdt_scores/bdt_EMu_wwz_vs_ttz.png", dpi=300)
