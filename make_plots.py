import uproot
import os
import glob
import numpy as np
import pandas as pd
import uproot_methods
from matplotlib import gridspec

from train_bdt import zz_features, ttz_features

labels = {
    "looper_MllN": r"$m_{ll}$ [GeV]",
    "looper_m_4l": r"$m_{4l}$ [GeV]",
    "looper_ZPt": r"$p_T^Z$ [GeV]",
    "looper_lep3MT": r"$m_T^{l3}$ [GeV]",
    "looper_lep4MT": r"$m_T^{l4}$ [GeV]",
    "met_pt": r"$p_T^{miss}$ [GeV]",
    "looper_pt_zeta": r"$P_\zeta$ [GeV]",
    "looper_pt_zeta_vis": r"$P_\zeta^{vis}$ [GeV]",
    "looper_vecsum_pt_4l": r"$p_T^{4l}$ vector sum [GeV]",
    "looper_scalarsum_pt_4l": r"$p_T^{4l}$ scalar sum [GeV]",
    "looper_minDRJetToLep3": r"min $\Delta R(j,l3)$",
    "looper_minDRJetToLep4": r"min $\Delta R(j,l4)$",
    "looper_jet1Pt": r"$p_T^{j1}$ [GeV]",
    "looper_lep3Pt": r"$p_T^{l3}$ [GeV]",
    "looper_lep4Pt": r"$p_T^{l4}$ [GeV]",
    "looper_mt2": r"$m_{T2}$ [GeV]",
}


def get_xlabel(feature):
    if feature in labels:
        return labels[feature]
    return feature


zz_features_and_binnings = {
    "looper_MllN": np.linspace(0, 500, 361),
    "looper_ZPt": np.linspace(0, 500, 91),
    "looper_mt2": np.linspace(0, 100, 46),
    "looper_lep3Pt": np.linspace(0, 300, 91),
    "looper_lep4Pt": np.linspace(0, 300, 91),
    "looper_vecsum_pt_4l": np.linspace(0, 600, 91),
    "looper_scalarsum_pt_4l": np.linspace(0, 800, 91),
    "looper_pt_zeta": np.linspace(-75, 300, 46),
    "looper_pt_zeta_vis": np.linspace(0, 400, 91),
    "met_pt": np.linspace(0, 180, 31),
    "looper_lep3MT": np.linspace(0, 180, 91),
    "looper_lep4MT": np.linspace(0, 180, 91),
    "looper_lep3dZ": np.linspace(0, 0.04, 31),
    "looper_lep4dZ": np.linspace(0, 0.04, 31),
    "looper_m_4l": np.linspace(175, 400, 91),
    # "bdt_EMu_wwz_vs_zz": np.linspace(-10, 10, 51),
    # "bdt_OffZ_wwz_vs_zz": np.linspace(-10, 10, 51),
    # "bdt_EMu_wwz_vs_ttz": np.linspace(-10, 10, 51),
    # "bdt_EMu_nonh_wwz_vs_zz": np.linspace(-10, 10, 51),
    # "bdt_OffZ_nonh_wwz_vs_zz": np.linspace(-10, 10, 51),
    # "bdt_EMu_nonh_wwz_vs_ttz": np.linspace(-10, 10, 51),

    # "EMu_ttz_score_wwz": np.linspace(-10, 10, 51),
    # "EMu_zz_score_wwz": np.linspace(-10, 10, 51),
    # "OffZ_zz_score_wwz": np.linspace(-10, 10, 51),
    # "EMu_ttz_score_nonh_wwz": np.linspace(-10, 10, 51),
    # "EMu_zz_score_nonh_wwz": np.linspace(-10, 10, 51),
    # "OffZ_zz_score_nonh_wwz": np.linspace(-10, 10, 51),
    # "EMu_bdt_bin": np.linspace(0, 5, 6),
    # "EMu_nonh_bdt_bin": np.linspace(0, 5, 6),
    # "bdt_bin": np.linspace(0, 7, 8),
    # "nonh_bdt_bin": np.linspace(0, 7, 8),
}

ttz_features_and_binnings = {
    "looper_MllN": np.linspace(0, 500, 46),
    "looper_ZPt": np.linspace(0, 500, 46),
    "looper_mt2": np.linspace(0, 100, 31),
    "looper_lep3Pt": np.linspace(0, 300, 46),
    "looper_lep4Pt": np.linspace(0, 300, 46),
    "looper_vecsum_pt_4l": np.linspace(0, 600, 46),
    "looper_scalarsum_pt_4l": np.linspace(0, 800, 46),
    "looper_minDRJetToLep3": np.linspace(0, 10, 46),
    "looper_minDRJetToLep4": np.linspace(0, 10, 46),
    "looper_jet1Pt": np.linspace(0, 500, 46),
    "looper_pt_zeta": np.linspace(-75, 300, 46),
    "looper_pt_zeta_vis": np.linspace(0, 400, 46),
    "met_pt": np.linspace(0, 180, 31),
    "looper_lep3MT": np.linspace(0, 180, 31),
    "looper_lep4MT": np.linspace(0, 180, 31),
    "looper_m_4l": np.linspace(175, 400, 46),
    # "bdt_EMu_wwz_vs_zz": np.linspace(-10, 10, 51),
    # "bdt_OffZ_wwz_vs_zz": np.linspace(-10, 10, 51),
    # "bdt_EMu_wwz_vs_ttz": np.linspace(-10, 10, 51),
    # "bdt_EMu_nonh_wwz_vs_zz": np.linspace(-10, 10, 51),
    # "bdt_OffZ_nonh_wwz_vs_zz": np.linspace(-10, 10, 51),
    # "bdt_EMu_nonh_wwz_vs_ttz": np.linspace(-10, 10, 51),

    # "EMu_ttz_score_wwz": np.linspace(-10, 10, 51),
    # "EMu_zz_score_wwz": np.linspace(-10, 10, 51),
    # "OffZ_zz_score_wwz": np.linspace(-10, 10, 51),
    # "EMu_ttz_score_nonh_wwz": np.linspace(-10, 10, 51),
    # "EMu_zz_score_nonh_wwz": np.linspace(-10, 10, 51),
    # "OffZ_zz_score_nonh_wwz": np.linspace(-10, 10, 51),
    # "EMu_bdt_bin": np.linspace(0, 5, 6),
    # "EMu_nonh_bdt_bin": np.linspace(0, 5, 6),
    # "bdt_bin": np.linspace(0, 7, 8),
    # "nonh_bdt_bin": np.linspace(0, 7, 8),
}

if __name__ == "__main__":

    import geeksw.plotting.cmsplot as plt
    from wvz_helpers import load_dataframes, load_dataframe, skims
    from syncronization import load_philips_yields
    import wvz_plotting

    print("Loading skims...")
    data = load_dataframes(skims.keys(), exclude_training=True)

    if not os.path.exists("plots/ChannelOnZ"):
        os.makedirs("plots/ChannelOnZ")

    # print("Make histograms in ChannelOnZ control region...")
    # for feature, bins in zz_features_and_binnings.items():
        # print(feature)
        # wvz_plotting.wvz_hist(
            # data,
            # feature,
            # bins,
            # query="is_ChannelOnZ",
            # main_background="zz",
            # plot_data=True,
            # ratio_plot=True,
            # xlabel=get_xlabel(feature),
        # )

        # plt.savefig("plots/ChannelOnZ/" + feature + ".png", dpi=300)
        # plt.savefig("plots/ChannelOnZ/" + feature + ".pdf")
        # plt.close()

    # if not os.path.exists("plots/ChannelBTagEMu"):
        # os.makedirs("plots/ChannelBTagEMu")

    # print("Make histograms in ChannelBTagEMu control region...")
    # for feature, bins in ttz_features_and_binnings.items():
        # print(feature)
        # wvz_plotting.wvz_hist(
            # data,
            # feature,
            # bins,
            # query="is_ChannelBTagEMu",
            # main_background="ttz",
            # plot_data=True,
            # ratio_plot=True,
            # xlabel=get_xlabel(feature),
        # )

        # plt.savefig("plots/ChannelBTagEMu/" + feature + ".png", dpi=300)
        # plt.savefig("plots/ChannelBTagEMu/" + feature + ".pdf")
        # plt.close()

    # if not os.path.exists("plots/ChannelOffZ"):
        # os.makedirs("plots/ChannelOffZ")

    # print("Make histograms in ChannelOffZ signal region...")
    # for feature, bins in zz_features_and_binnings.items():
        # print(feature)
        # wvz_plotting.wvz_hist(
            # data,
            # feature,
            # bins,
            # query="is_ChannelOffZ",
            # main_background="zz",
            # plot_data=True,
            # ratio_plot=False,
            # xlabel=get_xlabel(feature),
        # )

        # plt.savefig("plots/ChannelOffZ/" + feature + ".png", dpi=300)
        # plt.savefig("plots/ChannelOffZ/" + feature + ".pdf")
        # plt.close()

    if not os.path.exists("plots/ChannelEMu"):
        os.makedirs("plots/ChannelEMu")

    print("Make histograms in ChannelEMu control region...")
    for feature, bins in ttz_features_and_binnings.items():
        print(feature)
        wvz_plotting.wvz_hist(
            data,
            feature,
            bins,
            query="is_ChannelEMu",
            main_background="ttz",
            plot_data=True,
            ratio_plot=False,
            xlabel=get_xlabel(feature),
        )

        plt.savefig("plots/ChannelEMu/" + feature + ".png", dpi=300)
        plt.savefig("plots/ChannelEMu/" + feature + ".pdf")
        plt.close()
