#include <vector>
#include <iostream>
#include <unordered_map>
#include <stdexcept>

#include "TFile.h"
#include "TTree.h"

template <class T>
std::vector<T> getBranch(TTree& tree, const char* branchName) {
    std::vector<T> out;
    tree.SetBranchStatus("*", 0);
    tree.SetBranchStatus(branchName, 1);

    auto nEntries = tree.GetEntries();

    T x;
    tree.SetBranchAddress(branchName, &x);

    for (size_t i = 0; i < nEntries; ++i) {
        tree.GetEntry(i);
        out.push_back(x);
    }
    tree.SetBranchStatus("*", 0);

    return out;
}

std::vector<int> getEntriesCorrespondingToBabyEvents(std::vector<ULong64_t> nanoEvents,
                                                     std::unordered_set<ULong64_t> const& babyEventSet) {
    std::vector<int> nanoEntries;

    for (int i = 0; i < nanoEvents.size(); ++i) {
        if (babyEventSet.find(nanoEvents[i]) != babyEventSet.end()) {
            nanoEntries.push_back(i);
        }
    }

    return nanoEntries;
}

constexpr unsigned int g_maxCollectionSize{100};

template<class T>
void addTreeBranch(TTree& tree, const char* name,  T& variable) {
    tree.Branch(name, &variable);
}

#define TYPES_WITH_SUFFIX \
     X(char          , "/B") X(unsigned char , "/b") \
     X(short         , "/S") X(unsigned short, "/s") \
     X(int           , "/I") X(unsigned int  , "/i") \
     X(long          , "/L") X(unsigned long , "/l") \
     X(float         , "/F") X(double        , "/D") \
     X(char *        , "/C") X(bool , "/O")

#define X(Type, suffix) \
template<> \
void addTreeBranch<Type>(TTree& tree, const char* name, Type& variable) { \
    tree.Branch(name, &variable, (std::string(name) + suffix).c_str()); \
}
TYPES_WITH_SUFFIX
#undef X

#undef TYPES_WITH_SUFFIX


#define BABY_BRANCHES                  \
    X(Int_t, run)                      \
    X(Int_t, lumi)                     \
    X(ULong64_t, evt)                  \
    X(Int_t, isData)                   \
    X(Float_t, evt_scale1fb)           \
    X(Float_t, genps_weight)           \
    X(Float_t, xsec_br)                \
    X(Int_t, evt_passgoodrunlist)      \
    X(Float_t, weight_fr_r1_f1)        \
    X(Float_t, weight_fr_r1_f2)        \
    X(Float_t, weight_fr_r1_f0p5)      \
    X(Float_t, weight_fr_r2_f1)        \
    X(Int_t, nvtx)                     \
    X(Int_t, nTrueInt)                 \
    X(Int_t, firstgoodvertex)          \
    X(Int_t, passesMETfiltersRun2)     \
    X(Int_t, nj)                       \
    X(Int_t, nj_up)                    \
    X(Int_t, nj_dn)                    \
    X(Int_t, nb)                       \
    X(Int_t, nb_up)                    \
    X(Int_t, nb_dn)                    \
    X(Int_t, nbmed)                    \
    X(Float_t, ht)                       \
    X(Int_t, nj_cen)                   \
    X(Int_t, nj_cen_up)                \
    X(Int_t, nj_cen_dn)                \
    X(Float_t, weight_btagsf)          \
    X(Float_t, weight_btagsf_heavy_DN) \
    X(Float_t, weight_btagsf_heavy_UP) \
    X(Float_t, weight_btagsf_light_DN) \
    X(Float_t, weight_btagsf_light_UP)

struct BabyContent {
#define X(type, name) type name = 0;
    BABY_BRANCHES
#undef X
};

void setBabyBranchAddresses(TTree& tree, BabyContent& content) {
    tree.SetBranchStatus("*", 0);

#define X(_, name) tree.SetBranchStatus(#name, 1);
    BABY_BRANCHES
#undef X

#define X(_, name) tree.SetBranchAddress(#name, &(content.name));
    BABY_BRANCHES
#undef X
}

void setBabyBranches(TTree& tree, BabyContent& content) {
#define X(_, name) addTreeBranch(tree, #name, content.name);
    BABY_BRANCHES
#undef X
}

int nanoToBaby(std::vector<std::string> const& nanoFileNames,
               std::string outFileName,
               std::string const& babyReferenceFileName) {
    TFile babyFile(babyReferenceFileName.c_str(), "READ");
    TTree& baby = *dynamic_cast<TTree*>(babyFile.Get("t"));

    auto babyEvents = getBranch<ULong64_t>(baby, "evt");
    std::unordered_set<ULong64_t> babyEventSet;
    for (auto const& e : babyEvents) {
        babyEventSet.insert(e);
    }

    TFile outFile(outFileName.c_str(), "RECREATE");

    TTree tree("t", "All events");
    BabyContent content;
    setBabyBranches(tree, content);

    UInt_t runNano;
    UInt_t lumiNano;
    Int_t nvtxNano;
    Float_t nTrueIntNano;

    UInt_t nJet;
    std::vector<Float_t> jetPt(g_maxCollectionSize);
    std::vector<Float_t> jetRawFactor(g_maxCollectionSize);

    Bool_t flagMETFilters;

    for (auto nanoFileName : nanoFileNames) {
        TFile nanoFile(nanoFileName.c_str(), "READ");
        TTree& nano = *dynamic_cast<TTree*>(nanoFile.Get("Events"));

        auto nanoEvents = getBranch<ULong64_t>(nano, "event");
        auto nanoEntries = getEntriesCorrespondingToBabyEvents(nanoEvents, babyEventSet);

        nano.SetBranchStatus("*", 0);

        nano.SetBranchStatus("run", 1);
        nano.SetBranchStatus("luminosityBlock", 1);
        nano.SetBranchStatus("PV_npvsGood", 1);
        nano.SetBranchStatus("Pileup_nTrueInt", 1);
        nano.SetBranchStatus("nJet", 1);
        nano.SetBranchStatus("Jet_pt", 1);
        nano.SetBranchStatus("Jet_rawFactor", 1);
        nano.SetBranchStatus("Flag_METFilters", 1);

        nano.SetBranchAddress("run", &runNano);
        nano.SetBranchAddress("luminosityBlock", &lumiNano);
        nano.SetBranchAddress("PV_npvsGood", &nvtxNano);
        nano.SetBranchAddress("Pileup_nTrueInt", &nTrueIntNano);
        nano.SetBranchAddress("nJet", &nJet);
        nano.SetBranchAddress("Jet_pt", jetPt.data());
        nano.SetBranchAddress("Jet_rawFactor", jetRawFactor.data());
        nano.SetBranchAddress("Flag_METFilters", &flagMETFilters);

        for (auto const& i : nanoEntries) {
            nano.GetEntry(i);
            if (nJet > g_maxCollectionSize) {
                throw std::runtime_error("A collection size exceeded g_maxCollectionSize! You should increase it.");
            }
            content.evt = nanoEvents[i];
            content.run = runNano;
            content.lumi = lumiNano;
            content.nvtx = nvtxNano;
            content.nTrueInt = nTrueIntNano;

            content.firstgoodvertex = 0;
            content.passesMETfiltersRun2 = flagMETFilters;

            tree.Fill();
        }
    }

    outFile.Write();

    return 0;
}

int main() {
    std::string wvzDataPath = std::getenv("WVZ_DATA_PATH");

    std::string babyReferenceFileName = wvzDataPath + "/babies/WVZ2016_v0.1.12.7/wwz_amcatnlo_1.root";
    std::string outFileName = "out.root";
    std::vector<std::string> nanoFileNames{wvzDataPath + "/nano/RunIISummer16NanoAODv5/wwz_amcatnlo_1.root",
                                           wvzDataPath + "/nano/RunIISummer16NanoAODv5/wwz_amcatnlo_2.root"};

    nanoToBaby(nanoFileNames, outFileName, babyReferenceFileName);

    TFile babyFile(babyReferenceFileName.c_str(), "READ");
    TTree& baby = *dynamic_cast<TTree*>(babyFile.Get("t"));

    TFile outFile(outFileName.c_str(), "READ");
    TTree& tree = *dynamic_cast<TTree*>(outFile.Get("t"));

    if (baby.GetEntries() != tree.GetEntries()) {
        std::cerr << "The trees do not have the same length!" << std::endl;
        return 1;
    }

    auto babyEvents = getBranch<ULong64_t>(baby, "evt");
    std::unordered_map<ULong64_t, int> babyEventMap;
    for (int i = 0; i < babyEvents.size(); ++i) {
        babyEventMap[babyEvents[i]] = i;
    }

    BabyContent target;
    setBabyBranchAddresses(tree, target);
    BabyContent reference;
    setBabyBranchAddresses(baby, reference);

    std::vector<int> agreement{};
#define X(_, __) agreement.push_back(0);
    BABY_BRANCHES
#undef X

    for (int i = 0; i < tree.GetEntries(); ++i) {
        tree.GetEntry(i);
        baby.GetEntry(babyEventMap.at(target.evt));

        int iBranch = 0;
#define X(_, name)                       \
    if (target.name == reference.name) { \
        ++agreement[iBranch];            \
    }                                    \
    ++iBranch;
        BABY_BRANCHES
#undef X
    }
    int iBranch = 0;
#define X(_, name)                                                                                     \
    std::cout << #name << "\t" << 100.f * agreement[iBranch] / tree.GetEntries() << " %" << std::endl; \
    ++iBranch;
    BABY_BRANCHES
#undef X

    return 0;
}
