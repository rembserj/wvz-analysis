from io import StringIO

import pandas as pd

import urllib


def read_yields_from_url(target_url):
    def validate_line(s):
        letter_flag = False
        number_flag = False
        for i in s:
            if i.isalpha():
                letter_flag = True
            if i.isdigit():
                number_flag = True
        return letter_flag and number_flag

    text = urllib.request.urlopen(target_url).read().decode("utf-8")

    text = text.replace("|", ",")
    text = text.replace(" ", "")

    text = filter(validate_line, text.split("\n"))
    text = list(map(lambda line: line[1:-1], text))

    columns = [text[0].split(",")[0]]
    for component in text[0].split(",")[1:]:
        columns.append(component.lower())
        columns.append(component.lower() + "_err")

    text[0] = ",".join(columns)

    text = "\n".join(text)

    text = text.replace("±", ",")

    df = pd.read_csv(StringIO(text), index_col=0)

    yields = df[df.columns[::2]]
    yields.columns = map(lambda s: s[:-1] if s[-1].isdigit() else s, yields.columns)

    yields_err = df[df.columns[1::2]]
    yields_err.columns = yields.columns

    return yields, yields_err


def load_philips_yields(return_error=False):

    url_base = "/".join(
        [
            "http://uaf-10.t2.ucsd.edu/~phchang//analysis/wvz/WVZLooper/plots",
            "WVZMVA2016_v0.1.15_WVZMVA2017_v0.1.15_WVZMVA2018_v0.1.15",
            "y2016_nominal_nosmear_20191011_y2017_nominal_nosmear_20191011_y2018_nominal_nosmear_20191011",
            "yield",
        ]
    )

    yields_philip = {}
    yields_philip_err = {}

    channels = ["BTagEMu", "EMuHighMT", "EMu", "OffZHighMET", "OffZ", "OnZ"]

    for channel in channels:
        channel = "Channel" + channel
        res = read_yields_from_url(url_base + "/" + channel + "__Yield.txt")
        yields_philip[channel], yields_philip_err[channel] = res[0].loc["Bin1"], res[1].loc["Bin1"]

    if return_error:
        return yields_philip, yields_philip_err
    return yields_philip


if __name__ == "__main__":
    url_base = "/".join(
        [
            "http://uaf-10.t2.ucsd.edu/~phchang//analysis/wvz/WVZLooper/plots",
            "WVZMVA2016_v0.1.15_WVZMVA2017_v0.1.15_WVZMVA2018_v0.1.15",
            "y2016_nominal_nosmear_20191011_y2017_nominal_nosmear_20191011_y2018_nominal_nosmear_20191011",
            "yield",
        ]
    )

    print(url_base)
