#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd

from wvz_helpers import skims, load_dataframes

from geeksw.stats.binning import RectangularBinningModel


# In[2]:


nonh_only = True

if nonh_only:
    backgrounds = [
        "zz",
        "ttz",
        "twz",
        "wz",
        "higgs",
        "othernoh",
        "zh_zzz",
        "zh_wwz",
        "wh_wzz",
    ]
    signals = [
        "nonh_wwz",
        "nonh_wzz",
        "nonh_zzz",
    ]
else:
    backgrounds = [
        "zz",
        "ttz",
        "twz",
        "wz",
        "higgs",
        "othernoh",
    ]
    signals = [
        "nonh_wwz",
        "nonh_wzz",
        "nonh_zzz",
        "zh_zzz",
        "zh_wwz",
        "wh_wzz",
    ]


# In[3]:


data_background = load_dataframes(backgrounds, concat=True, exclude_training=True)
data_signal = load_dataframes(signals, concat=True, exclude_training=True)


# In[5]:


data_background["is_signal"] = False
data_signal["is_signal"] = True


# In[6]:


data = pd.concat([data_signal, data_background], ignore_index=True)
data = data.query("is_ChannelEMu").copy()


# In[7]:


if nonh_only:
    X = data[["EMu_zz_score_nonh_wwz", "EMu_ttz_score_nonh_wwz"]].values
else:
    X = data[["EMu_zz_score_wwz", "EMu_ttz_score_wwz"]].values


# In[8]:


y = data.eval("is_signal == 1").values
w = data["weight"].values


# In[9]:


data.query("is_signal == 1")["weight"].sum()


# In[10]:


data.query("is_signal == 0")["weight"].sum()


# In[11]:


if nonh_only:
    box_model = RectangularBinningModel(
        max_depth=5, snap_boxes_to_full_phasespace=False, override_splitting_test=True
    ).fit(X, y, sample_weight=w)
else:
    box_model = RectangularBinningModel(max_depth=5, snap_boxes_to_full_phasespace=True).fit(X, y, sample_weight=w)


# In[12]:


data["fitting_bin"] = box_model.apply(X)


# In[13]:


def L(s, b):
    return np.sqrt(2.0 * ((s + b) * np.log(1 + s / b) - s))


# In[14]:


def make_summary_df(data):
    data_signal = data.query("is_signal == 1")
    data_background = data.query("is_signal == 0")
    df = pd.DataFrame()
    df["s"] = data_signal.groupby("fitting_bin")["weight"].sum()
    df["b"] = data_background.groupby("fitting_bin")["weight"].sum()
    df["s/b"] = df["s"] / df["b"]
    df["s_rel_uncert [%]"] = 1.0 / np.sqrt(data_signal.groupby("fitting_bin")["weight"].count()) * 100
    df["b_rel_uncert [%]"] = 1.0 / np.sqrt(data_background.groupby("fitting_bin")["weight"].count()) * 100
    df["L"] = L(df["s"], df["b"])
    df.loc["total", ["s", "b"]] = df[["s", "b"]].sum()
    df.loc["combined", "L"] = np.sqrt((df["L"].dropna() ** 2).sum())
    df.loc["total", "s_rel_uncert [%]"] = 1.0 / np.sqrt(data_signal["weight"].count()) * 100
    df.loc["total", "b_rel_uncert [%]"] = 1.0 / np.sqrt(data_background["weight"].count()) * 100
    return df


# In[15]:


df = make_summary_df(data)
print(df)


# In[17]:

new_boxes = box_model.boxes_.copy(deep=True)

# Manual hacking because the box model does not work as it should sometimes

if nonh_only:
    new_boxes.loc[0, "xmin"] = -np.inf
    new_boxes.loc[0, "xmax"] = new_boxes.loc[1, "xmin"]
    new_boxes.loc[1, "ymin"] = new_boxes.loc[2, "ymax"]
    new_boxes.loc[2, "xmin"] = new_boxes.loc[0, "xmax"]
    new_boxes.loc[2, "xmax"] = np.inf
    new_boxes.loc[3, "xmin"] = new_boxes.loc[1, "xmax"]
    new_boxes.loc[3, "xmax"] = np.inf
    new_boxes.loc[4, "xmin"] = new_boxes.loc[1, "xmax"]
    new_boxes.loc[4, "xmax"] = np.inf
    new_boxes = new_boxes.drop([5, 6])
else:
    new_boxes = box_model.boxes_.copy(deep=True)
    new_boxes.loc[1, "ymin"] = new_boxes.loc[3, "ymax"]
    new_boxes.loc[1, "ymax"] = np.inf
    new_boxes.loc[2, "xmin"] = new_boxes.loc[0, "xmax"]
    new_boxes.loc[2, "xmax"] = np.inf
    new_boxes.loc[2, "ymax"] = new_boxes.loc[3, "ymax"]
    new_boxes = new_boxes.drop([3])

new_boxes.index = range(len(new_boxes))

print(new_boxes)

# In[18]:


manual_box_model = RectangularBinningModel.from_pandas(new_boxes)
data["fitting_bin"] = manual_box_model.apply(X)

if nonh_only:
    new_boxes.columns = ["zz_score_min", "zz_score_max", "ttz_score_min", "ttz_score_max"]
    new_boxes.to_csv("plots/EMu_2D_bins_nonh.csv")
else:
    new_boxes.columns = ["zz_score_min", "zz_score_max", "ttz_score_min", "ttz_score_max"]
    new_boxes.to_csv("plots/EMu_2D_bins.csv")

# In[19]:


df = make_summary_df(data)
print(df)


# In[20]:


# formatter = lambda x: "{:.2f}".format(x) if not np.isnan(x) else "--"
# df.to_html("plots/EMu_2D_yields.html", formatters=[formatter] * len(df.columns))


# In[21]:


plt.figure()
bins = np.linspace(-10, 10, 200)
plt.hist2d(
    X[y == 0, 0], X[y == 0, 1], weights=w[y == 0], bins=[bins, bins], norm=mpl.colors.LogNorm(), cmap=mpl.cm.Blues
)
plt.colorbar()
manual_box_model.visualize(alpha=0.5)
plt.legend(loc="lower left")
plt.title("Background")
plt.ylabel("ttZ score")
plt.xlabel("zz score")
if nonh_only:
    plt.savefig("plots/emu_bdt_2d_background_nonh.pdf")
    plt.savefig("plots/emu_bdt_2d_background_nonh.png", dpi=300)
else:
    plt.savefig("plots/emu_bdt_2d_background.pdf")
    plt.savefig("plots/emu_bdt_2d_background.png", dpi=300)
plt.close()


# In[22]:


plt.figure()
bins = np.linspace(-10, 10, 200)
plt.hist2d(
    X[y == 1, 0], X[y == 1, 1], weights=w[y == 1], bins=[bins, bins], norm=mpl.colors.LogNorm(), cmap=mpl.cm.Reds
)
plt.colorbar()
manual_box_model.visualize(alpha=0.5)
plt.legend(loc="lower left")
plt.title("Signal")
plt.ylabel("ttZ score")
plt.xlabel("zz score")
if nonh_only:
    plt.savefig("plots/emu_bdt_2d_signal_nonh.pdf")
    plt.savefig("plots/emu_bdt_2d_signal_nonh.png", dpi=300)
else:
    plt.savefig("plots/emu_bdt_2d_signal.pdf")
    plt.savefig("plots/emu_bdt_2d_signal.png", dpi=300)
plt.close()


# In[24]:


import pickle

if nonh_only:
    with open("models/EMu2DBinning_nonh.pickle", "wb") as handle:
        pickle.dump(manual_box_model, handle, protocol=pickle.HIGHEST_PROTOCOL)
else:
    with open("models/EMu2DBinning.pickle", "wb") as handle:
        pickle.dump(manual_box_model, handle, protocol=pickle.HIGHEST_PROTOCOL)
