import numpy as np
import xgboost as xgb

from wvz_helpers import load_dataframes, load_dataframe, skims

import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn import metrics

import os


test_size = 0.33
random_state = 42

common_features = [
    "looper_MllN",
    "looper_ZPt",
    "looper_mt2",
    "looper_lep3Pt",
    "looper_lep4Pt",
    "looper_vecsum_pt_4l",
    "looper_scalarsum_pt_4l",
]
zz_features = common_features + [
    "looper_pt_zeta",
    "looper_pt_zeta_vis",
    "met_pt",
    "looper_lep3MT",
    "looper_lep4MT",
    "looper_m_4l",
]
ttz_features = common_features + ["looper_minDRJetToLep3", "looper_minDRJetToLep4", "looper_jet1Pt"]


xgb_params_emu = dict(max_depth=3, learning_rate=0.1, n_estimators=400, reg_lambda=1.0)
xgb_params_offz = dict(max_depth=5, learning_rate=0.1, n_estimators=400, reg_lambda=1.0)


def train_bdt(df, features, xgb_params, signal="wwz", max_samples=None):

    # reshuffle
    df = df.sample(frac=1)

    if not max_samples is None and len(df) > max_samples:
        df = df[:max_samples]

    X_train = df[features]
    y_train = df["sample"] == signal

    n_pos = np.sum(y_train)
    n_neg = len(y_train) - n_pos
    print("n_pos: " + str(n_pos))
    print("n_neg: " + str(n_neg))
    scale_pos_weight = 1.0 * n_neg / n_pos

    model = xgb.XGBClassifier(
        scale_pos_weight=scale_pos_weight,
        overbosity=2,
        n_jobs=4,
        random_state=random_state,
        objective="binary:logitraw",
        **xgb_params,
    )
    model.fit(X_train, y_train)

    return model


def save_bdt(model, filename_without_suffix):
    print(f"saving model to {filename_without_suffix}.bin and {filename_without_suffix}.txt")
    model.save_model(filename_without_suffix + ".bin")
    model.get_booster().dump_model(filename_without_suffix + ".txt")


from dataclasses import dataclass


@dataclass(frozen=True)
class BTDTraining:
    name: str
    description: str
    features: list
    train_query: str
    test_query: str
    signal: str
    xgb_params: dict
    train_query_for_roc: str = None


signal = "wwz"  # or "nonh_wwz"

trainings = [
    BTDTraining(
        "emu_zz",
        description=r"WWZ vs. ZZ in $e\mu$ category",
        features=zz_features,
        train_query=f"evt % 3 != 0 and is_ChannelEMu and (sample == 'zz' or sample == '{signal}')",
        test_query=f"evt % 3 == 0 and is_ChannelEMu and (sample == 'zz' or sample == '{signal}')",
        signal=signal,
        xgb_params=xgb_params_emu,
    ),
    BTDTraining(
        "offz_zz",
        description=r"WWZ vs. ZZ in off-Z category",
        features=zz_features,
        train_query=f"evt % 3 != 0 and is_ChannelOffZ and (sample == 'zz' or sample == '{signal}')",
        test_query=f"evt % 3 == 0 and is_ChannelOffZ and (sample == 'zz' or sample == '{signal}')",
        signal=signal,
        xgb_params=xgb_params_offz,
    ),
    BTDTraining(
        "emu_ttz",
        description=r"WWZ vs. ttZ in $e\mu$ category",
        features=ttz_features,
        # train_query=f"evt % 3 != 0 and (sample == 'ttz' or (sample == '{signal}' and is_ChannelEMu))",
        # train_query_for_roc=f"evt % 3 != 0 and is_ChannelEMu and (sample == 'ttz' or sample == '{signal}')",
        train_query=f"evt % 3 != 0 and is_ChannelEMu and (sample == 'ttz' or sample == '{signal}')",
        test_query=f"evt % 3 == 0 and is_ChannelEMu and (sample == 'ttz' or sample == '{signal}')",
        signal=signal,
        xgb_params=xgb_params_emu,
    ),
]


def plot_importance(
    importance,
    ax=None,
    height=0.2,
    xlim=None,
    ylim=None,
    title="Feature importance",
    xlabel="F score",
    ylabel="Features",
    max_num_features=None,
    normalize=False,
    lower_is_better=False,
    grid=True,
    show_values=True,
    **kwargs,
):

    max_importance = max(importance.values())

    tuples = [(k, importance[k] / (max_importance if normalize else 1.0)) for k in importance]

    factor = -1 if lower_is_better else 1

    if max_num_features is not None:
        # pylint: disable=invalid-unary-operand-type
        tuples = sorted(tuples, key=lambda x: factor * x[1])[-max_num_features:]
    else:
        tuples = sorted(tuples, key=lambda x: factor * x[1])
    labels, values = zip(*tuples)

    if ax is None:
        _, ax = plt.subplots(1, 1)

    ylocs = np.arange(len(values))
    ax.barh(ylocs, values, align="center", height=height, **kwargs)

    if show_values is True:
        # for x, y in zip(values, ylocs):
        for x, y, l in zip(values, ylocs, labels):
            ax.text(x + max_importance * 0.05, y, l, va="center")
            # ax.text(1 + 0.05, y, l, va='center')

    ax.set_yticks(ylocs)
    ax.set_yticklabels([len(labels) - x for x in range(len(labels))])

    if xlim is not None:
        if not isinstance(xlim, tuple) or len(xlim) != 2:
            raise ValueError("xlim must be a tuple of 2 elements")
    else:
        # xlim = (0, max(values) * 1.1)
        xlim = (0, max(values) * 1.3)
    ax.set_xlim(xlim)

    if ylim is not None:
        if not isinstance(ylim, tuple) or len(ylim) != 2:
            raise ValueError("ylim must be a tuple of 2 elements")
    else:
        ylim = (-1, len(values))
    ax.set_ylim(ylim)

    if title is not None:
        ax.set_title(title)
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)
    ax.grid(grid)
    return ax


if __name__ == "__main__":

    model_dir = "models_" + signal
    entrystop = None  # set so a finite number for testing, like 10000
    make_roc_curves = False
    save_bdts = True
    save_scores = True
    make_feature_importance_plots = False
    n_minus_one_study = False

    print("Loading data...")
    df = load_dataframes([signal, "ttz", "zz"], concat=True, entrystop=entrystop)

    import os
    import pandas as pd

    from make_plots import labels as latex_labels

    if not os.path.exists(model_dir):
        os.makedirs(model_dir)

    if not os.path.exists("plots"):
        os.makedirs("plots")

    def plot_roc(model, features, query, label=None):
        df_q = df.query(query)
        preds = model.predict_proba(df_q[features])[:, 1]
        fpr, tpr, _ = roc_curve(df_q["sample"] == signal, preds)
        print("AUC:", metrics.auc(fpr, tpr))
        plt.plot(tpr[tpr > 0.4], fpr[tpr > 0.4], label=label)

    models = dict()

    for training in trainings:
        print(f"Training {training.name} ({training.description})")
        model = train_bdt(df.query(training.train_query), training.features, training.xgb_params, training.signal)

        auc_without_feature = dict()

        if n_minus_one_study:

            if not os.path.exists(model_dir + "/n_minus_one/"):
                os.makedirs(model_dir + "/n_minus_one/")

            df_test = df.query(training.test_query)
            preds = model.predict_proba(df_test[training.features])[:, 1]
            fpr, tpr, _ = roc_curve(df_test["sample"] == signal, preds)
            full_auc = metrics.auc(fpr, tpr)

            for feat in training.features:
                print(f"training withoug {feat}...")
                reduced_features = [f for f in training.features if not f == feat]
                model_without_feature = train_bdt(
                    df.query(training.train_query), reduced_features, training.xgb_params, training.signal
                )

                if save_bdts:
                    save_bdt(model_without_feature, model_dir + "/n_minus_one/" + training.name + "_bdt_no_" + feat)

                preds = model_without_feature.predict_proba(df_test[reduced_features])[:, 1]
                fpr, tpr, _ = roc_curve(df_test["sample"] == signal, preds)
                auc_without_feature[feat] = metrics.auc(fpr, tpr)

            print("AUC: " + str(full_auc))
            print("AUC without features:")
            print(auc_without_feature)

        booster = model.get_booster()

        score = booster.get_score()

        if make_feature_importance_plots:
            if not os.path.exists("plots/feature_importance"):
                os.makedirs("plots/feature_importance")
            for importance_type in ["weight", "gain", "cover", "n_minus_one"]:
                if importance_type == "n_minus_one":
                    if not n_minus_one_study:
                        continue
                    importance = {k: max(0, full_auc - v) for k, v in auc_without_feature.items()}
                else:
                    importance = importance = booster.get_score(importance_type=importance_type)
                importance = {latex_labels[k].replace("[GeV]", "").strip(): v for k, v in importance.items()}

                xlabel = importance_type
                xlim = None
                normalize = False
                lower_is_better = False
                if importance_type == "n_minus_one":
                    xlabel = "AUC decrease by leaving feature out"
                    # xlim = (0,1.4)
                    # normalize = False
                    # lower_is_better = True

                plt.figure()
                plot_importance(
                    importance,
                    height=0.5,
                    ax=plt.gca(),
                    grid=False,
                    show_values=True,
                    ylabel=None,
                    xlabel=xlabel,
                    title="Feature importance for " + training.description,
                    xlim=xlim,
                    normalize=normalize,
                    lower_is_better=lower_is_better,
                )
                filename = f"plots/feature_importance/{training.name}_{signal}_{importance_type}"

                # if importance_type == "n_minus_one":
                #    plt.plot([full_auc, full_auc], plt.ylim(), 'k--', label="all features")
                #    plt.legend(loc="upper left")

                plt.savefig(filename + ".pdf")
                plt.savefig(filename + ".png", dpi=300, bbox_inches="tight")
                plt.close()

        if save_bdts:
            save_bdt(model, model_dir + "/" + training.name + "_bdt")

        if make_roc_curves:
            plt.figure()
            train_query = training.train_query_for_roc
            if train_query is None:
                train_query = training.train_query
            plot_roc(model, training.features, train_query, label="train")
            plot_roc(model, training.features, training.test_query, label="test")
            plt.legend()
            plt.title(training.description)
            plt.xlim(0.4, 1)
            plt.ylim(0, 1)
            plt.ylabel("background efficiency")
            plt.xlabel("signal efficiency")
            plt.savefig(f"plots/{training.name}_{signal}_bdt_roc.pdf")
            plt.savefig(f"plots/{training.name}_{signal}_bdt_roc.png", dpi=300)
            plt.close()

    if save_scores:
        if not os.path.exists(f"bdt_scores_{signal}"):
            os.makedirs(f"bdt_scores_{signal}")

        print("Evaluating BDTs...")
        for sample in skims.keys():
            print("   processing " + sample + "...")
            try:
                df_sample = load_dataframe(sample)
            except:
                print("sample " + sample + " could not be evaluated")
                continue

            emu_ttz_scores = models["emu_ttz"].predict_proba(df_sample[ttz_features])[:, 1]
            emu_zz_scores = models["emu_zz"].predict_proba(df_sample[zz_features])[:, 1]
            offz_zz_scores = models["offz_zz"].predict_proba(df_sample[zz_features])[:, 1]

            df_score = pd.DataFrame(
                {
                    f"emu_{signal}_vs_ttz_score": emu_ttz_scores,
                    f"emu_{signal}_vs_zz_score": emu_zz_scores,
                    f"offz_{signal}_vs_zz_score": offz_zz_scores,
                }
            )

            df_score.to_hdf(f"bdt_scores_{signal}/" + sample + ".hdf", key="bdt_scores")
