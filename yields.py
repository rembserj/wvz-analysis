import pandas as pd

from wvz_helpers import load_dataframes
from syncronization import load_philips_yields

import numpy as np

from geeksw.utils.pd_utils import format_errors_in_df

# yields_philip = load_philips_yields()


def L(s, b):
    return np.sqrt(2.0 * ((s + b) * np.log(1 + s / b) - s))


# def complete(y):
# y = y.copy()
# if "total" in y:
# y = y.drop(["total", "data", "ratio"])

# tot = y.sum()

# sig = y["wwz"] + y["wzz"] + y["zzz"]
# y["total_sig"] = sig
# y["total_bkg"] = tot - sig
# y["L"] = L(sig, tot - sig)

# return y


# yields = [
# yields_philip["ChannelEMu"],
# yields_philip["ChannelEMuHighMT"],
# yields_philip["ChannelBTagEMu"],
# yields_philip["ChannelOnZ"],
# yields_philip["ChannelOffZ"],
# yields_philip["ChannelOffZHighMET"],
# ]


# yields = pd.concat([complete(y) for y in yields], axis=1, sort=False)
# yields.columns = [
# "EMu",
# "EMuHighMT",
# "BTagEMu",
# "OnZ",
# "OffZ",
# "OffZHighMET",
# ]
# yields = yields.transpose()

html = '<head><meta charset="UTF-8"></head>'
formatter = lambda x: "{:.2f}".format(x)
# print(yields)
# print()
# html += "</br><h2>Yields Philip</h2>"
# html += yields.to_html(formatters=[formatter] * len(yields.columns))


yields_jonas = dict(
    ChannelEMu=pd.Series(),
    ChannelBTagEMu=pd.Series(),
    ChannelEMuHighMT=pd.Series(),
    ChannelEMuBDT0=pd.Series(),
    ChannelEMuBDT1=pd.Series(),
    ChannelEMuBDT2=pd.Series(),
    ChannelEMuBDT3=pd.Series(),
    ChannelEMuBDT4=pd.Series(),
    ChannelOnZ=pd.Series(),
    ChannelOffZ=pd.Series(),
    ChannelOffZBDT0=pd.Series(),
    ChannelOffZBDT1=pd.Series(),
    ChannelOffZHighMET=pd.Series(),
)

yields_map = {
    "zz": ["zz"],
    "ttz": ["ttz"],
    "twz": ["twz"],
    "wz": ["wz"],
    "higgs": ["higgs"],
    "othernoh": ["rare", "dy", "ttbar"],
    "wwz": ["wwz"],
    "wzz": ["wzz"],
    "zzz": ["zh_zzz", "nonh_zzz"],
    "nonh_wwz": ["nonh_wwz"],
    "nonh_wzz": ["nonh_wzz"],
    "nonh_zzz": ["nonh_zzz"],
    "vh_wwz": ["zh_wwz"],
    "vh_wzz": ["wh_wzz"],
    "vh_zzz": ["zh_zzz"],
}

nonh_only = False

for label, samples in yields_map.items():

    if label in ["wwz", "wzz", "zzz"]:
        continue

    sums = {}

    data = load_dataframes(samples, exclude_training=True)

    for sample in samples:

        df = data[sample]

        def fill(bin_name, query, scale=1.0):
            if not bin_name + "_weight" in sums:
                sums[bin_name + "_weight"] = 0.0
            if not bin_name + "_w2" in sums:
                sums[bin_name + "_w2"] = 0.0

            df_q = df.query(query)
            sums[bin_name + "_weight"] += scale * df_q["weight"].sum()
            sums[bin_name + "_w2"] += scale * (df_q["weight"] ** 2).sum()

        fill("ChannelEMu", "is_ChannelEMu")
        fill("ChannelBTagEMu", "is_ChannelBTagEMu")
        fill("ChannelOnZ", "is_ChannelOnZ")
        fill("ChannelOffZ", "is_ChannelOffZ")
        fill("ChannelOffZHighMET", "is_ChannelOffZ and met_orig_pt > 100.")
        fill("ChannelEMuHighMT", "is_ChannelEMuHighMT")

        if nonh_only:
            fill("ChannelEMuBDT0", "is_ChannelEMu and EMu_nonh_bdt_bin == 0")
            fill("ChannelEMuBDT1", "is_ChannelEMu and EMu_nonh_bdt_bin == 1")
            fill("ChannelEMuBDT2", "is_ChannelEMu and EMu_nonh_bdt_bin == 2")
            fill("ChannelEMuBDT3", "is_ChannelEMu and EMu_nonh_bdt_bin == 3")
            fill("ChannelEMuBDT4", "is_ChannelEMu and EMu_nonh_bdt_bin == 4")

            fill("ChannelOffZBDT0", "is_ChannelOffZ and OffZ_zz_score_nonh_wwz <= 3.5")
            fill("ChannelOffZBDT1", "is_ChannelOffZ and OffZ_zz_score_nonh_wwz > 3.5")
        else:
            fill("ChannelEMuBDT0", "is_ChannelEMu and EMu_bdt_bin == 0")
            fill("ChannelEMuBDT1", "is_ChannelEMu and EMu_bdt_bin == 1")
            fill("ChannelEMuBDT2", "is_ChannelEMu and EMu_bdt_bin == 2")
            fill("ChannelEMuBDT3", "is_ChannelEMu and EMu_bdt_bin == 3")
            fill("ChannelEMuBDT4", "is_ChannelEMu and EMu_bdt_bin == 4")

            fill("ChannelOffZBDT0", "is_ChannelOffZ and OffZ_zz_score_wwz <= 3.0")
            fill("ChannelOffZBDT1", "is_ChannelOffZ and OffZ_zz_score_wwz > 3.0")

    for channel in yields_jonas:
        yields_jonas[channel][label] = sums[channel + "_weight"]
        yields_jonas[channel][label + "_w2"] = sums[channel + "_w2"]


def complete(y):
    print(y)
    y = y.copy().fillna(0)

    tot = y[[c for c in y.index if not "_w2" in c]].sum()
    tot_w2 = y[[c for c in y.index if "_w2" in c]].sum()

    vvv = y["nonh_wwz"] + y["nonh_wzz"] + y["nonh_zzz"]
    vh = y["vh_wwz"] + y["vh_wzz"] + y["vh_zzz"]

    vvv_w2 = y["nonh_wwz_w2"] + y["nonh_wzz_w2"] + y["nonh_zzz_w2"]
    vh_w2 = y["vh_wwz_w2"] + y["vh_wzz_w2"] + y["vh_zzz_w2"]

    y["total_sig_nonh"] = vvv
    y["total_sig_nonh_w2"] = vvv_w2

    y["total_sig_vh"] = vh
    y["total_sig_vh_w2"] = vh_w2

    y["total_sig"] = vvv + vh
    y["total_sig_w2"] = vvv_w2 + vh_w2

    y["total_bkg"] = tot - vvv - vh
    y["total_bkg_w2"] = tot_w2 - vvv_w2 - vh_w2

    y["L"] = L(vvv + vh, tot - vvv - vh)
    y["L (no VH)"] = L(vvv, tot - vvv)

    for idx in y.index:
        if "_w2" in idx:
            y[idx.replace("_w2", "_err")] = np.sqrt(y[idx])

    return y.drop([idx for idx in y.index if "_w2" in idx])


yields = [
    yields_jonas["ChannelEMu"],
    yields_jonas["ChannelEMuHighMT"],
    yields_jonas["ChannelBTagEMu"],
    yields_jonas["ChannelEMuBDT0"],
    yields_jonas["ChannelEMuBDT1"],
    yields_jonas["ChannelEMuBDT2"],
    yields_jonas["ChannelEMuBDT3"],
    yields_jonas["ChannelEMuBDT4"],
    yields_jonas["ChannelOnZ"],
    yields_jonas["ChannelOffZ"],
    yields_jonas["ChannelOffZBDT0"],
    yields_jonas["ChannelOffZBDT1"],
    yields_jonas["ChannelOffZHighMET"],
]


yields = pd.concat([complete(y) for y in yields], axis=1, sort=False)
yields.columns = [
    "EMu",
    "EMuHighMT",
    "BTagEMu",
    "EMuBDT0",
    "EMuBDT1",
    "EMuBDT2",
    "EMuBDT3",
    "EMuBDT4",
    "OnZ",
    "OffZ",
    "OffZBDT0",
    "OffZBDT1",
    "OffZHighMET",
]
yields = yields.transpose()

print(format_errors_in_df(yields))
print()

html += "</br><h2>Yields in Cut Bins</h2>"
# html += format_errors_in_df(yields).to_html(formatters=[formatter] * len(yields.columns))
# formatters = ([lambda x : x] * (len(yields.columns)-2)) + (2 * [formatter])
# print(formatters)
yields_cut = yields.loc[[c for c in yields.index if not "BDT" in c]]
html += "</br><h3>Signal Yields</h3>"
signals = [
    "nonh_wwz",
    "nonh_wzz",
    "nonh_zzz",
    "vh_wwz",
    "vh_wzz",
    "vh_zzz",
    "total_sig_nonh",
    "total_sig_vh",
    "total_sig",
    "total_bkg",
]
html += format_errors_in_df(yields_cut[signals + [s + "_err" for s in signals] + ["L", "L (no VH)"]]).to_html()
html += "</br><h3>Background Composition</h3>"
backgrounds = ["zz", "ttz", "twz", "wz", "higgs", "othernoh"]
html += format_errors_in_df(yields_cut[backgrounds + [b + "_err" for b in backgrounds]]).to_html()

if nonh_only:
    yields.to_csv("plots/yields_nonh.csv")
else:
    yields.to_csv("plots/yields.csv")

html += "</br><h2>Yields in BDT Bins</h2>"
html += "<p>The BDT bins are mutually exclusive and are the bins that should be used in the global fit.</p>"
# html += format_errors_in_df(yields).to_html(formatters=[formatter] * len(yields.columns))
# formatters = ([lambda x : x] * (len(yields.columns)-2)) + (2 * [formatter])
# print(formatters)
yields_bdt = yields.loc[[c for c in yields.index if "BDT" in c]]
html += "</br><h3>Signal Yields</h3>"
signals = [
    "nonh_wwz",
    "nonh_wzz",
    "nonh_zzz",
    "vh_wwz",
    "vh_wzz",
    "vh_zzz",
    "total_sig_nonh",
    "total_sig_vh",
    "total_sig",
    "total_bkg",
]
html += format_errors_in_df(yields_bdt[signals + [s + "_err" for s in signals] + ["L", "L (no VH)"]]).to_html()
html += "</br><h3>Background Composition</h3>"
backgrounds = ["zz", "ttz", "twz", "wz", "higgs", "othernoh"]
html += format_errors_in_df(yields_bdt[backgrounds + [b + "_err" for b in backgrounds]]).to_html()

summary = pd.DataFrame(index=["cut-based-1-bin", "BDTs"])
summary.loc["cut-based-1-bin", "L"] = np.sqrt(yields.loc["EMuHighMT", "L"] ** 2 + yields.loc["OffZHighMET", "L"] ** 2)
summary.loc["cut-based-1-bin", "L (no VH)"] = np.sqrt(
    yields.loc["EMuHighMT", "L (no VH)"] ** 2 + yields.loc["OffZHighMET", "L (no VH)"] ** 2
)
summary.loc["BDTs", "L"] = np.sqrt(
    yields.loc["EMuBDT0", "L"] ** 2
    + yields.loc["EMuBDT1", "L"] ** 2
    + yields.loc["EMuBDT3", "L"] ** 2
    + yields.loc["EMuBDT4", "L"] ** 2
    + yields.loc["EMuBDT2", "L"] ** 2
    + yields.loc["OffZBDT0", "L"] ** 2
    + yields.loc["OffZBDT1", "L"] ** 2
)
summary.loc["BDTs", "L (no VH)"] = np.sqrt(
    yields.loc["EMuBDT0", "L (no VH)"] ** 2
    + yields.loc["EMuBDT1", "L (no VH)"] ** 2
    + yields.loc["EMuBDT3", "L (no VH)"] ** 2
    + yields.loc["EMuBDT4", "L (no VH)"] ** 2
    + yields.loc["EMuBDT2", "L (no VH)"] ** 2
    + yields.loc["OffZBDT0", "L (no VH)"] ** 2
    + yields.loc["OffZBDT1", "L (no VH)"] ** 2
)

html += "</br><h2>Combined Sensitivities</h2>"
html += "<p>For the BDT analysis, all of the channels that contain BDT in the name are combined.</p>"
print(summary)
print()

html += summary.to_html(formatters=[formatter] * len(summary.columns))

if nonh_only:
    with open("plots/yields_nonh.html", "w+") as f:
        f.write(html)
else:
    with open("plots/yields.html", "w+") as f:
        f.write(html)
