import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd

from wvz_helpers import skims, load_dataframes

nonh_only = False

if nonh_only:

    backgrounds = [
        "zz",
        "ttz",
        "twz",
        "wz",
        "higgs",
        "othernoh",
        "zh_wwz",
        "wh_wzz",
        "zh_zzz",
    ]
    signals = [
        "nonh_wwz",
        "nonh_wzz",
        "nonh_zzz",
    ]
else:
    backgrounds = [
        "zz",
        "ttz",
        "twz",
        "wz",
        "higgs",
        "othernoh",
    ]
    signals = [
        "nonh_wwz",
        "zh_wwz",
        "nonh_wzz",
        "wh_wzz",
        "nonh_zzz",
        "zh_zzz",
    ]

data_background = load_dataframes(backgrounds, concat=True, exclude_training=True)
data_signal = load_dataframes(signals, concat=True, exclude_training=True)

data_background["is_signal"] = False
data_signal["is_signal"] = True

data = pd.concat([data_signal, data_background], ignore_index=True)
data = data.query("is_ChannelOffZ").copy()

if nonh_only:
    bdt_col = "OffZ_zz_score_nonh_wwz"
else:
    bdt_col = "OffZ_zz_score_wwz"

X = data[[bdt_col]].values

y = data.eval("is_signal == 1").values
w = data["weight"].values

data.query("is_signal == 1")["weight"].sum()

data.query("is_signal == 0")["weight"].sum()


def L(s, b):
    return np.sqrt(2.0 * ((s + b) * np.log(1 + s / b) - s))


cut_range = np.linspace(-1, 5, 50)

l_range = np.zeros_like(cut_range)


def estimate_l(cut):
    sig = data.query(f"is_signal == 1 and {bdt_col} <= {cut}")["weight"].sum()
    bkg = data.query(f"is_signal == 0 and {bdt_col} <= {cut}")["weight"].sum()
    l1 = L(sig, bkg)
    sig = data.query(f"is_signal == 1 and {bdt_col} > {cut}")["weight"].sum()
    bkg = data.query(f"is_signal == 0 and {bdt_col} > {cut}")["weight"].sum()
    l2 = L(sig, bkg)
    return (l1 ** 2 + l2 ** 2) ** 0.5


for i, cut in enumerate(cut_range):
    l_range[i] = estimate_l(cut)

plt.plot(cut_range, l_range, label="first split")
plt.xlabel("OffZ BDT cut")
plt.ylabel("Significance estimate L")

if nonh_only:
    plt.savefig("plots/offz_bdt_significance_scan_nonh.pdf")
    plt.savefig("plots/offz_bdt_significance_scan_nonh.png", dpi=300)
else:
    plt.savefig("plots/offz_bdt_significance_scan.pdf")
    plt.savefig("plots/offz_bdt_significance_scan.png", dpi=300)

# make csv file with cut
df = pd.DataFrame(dict(zz_score_min=[-np.inf, 3.0], zz_score_max=[3.0, np.inf]))
df.to_csv("plots/OffZ_bins.csv")

cut_range = np.linspace(0, 5, 50)

l_range = np.zeros_like(cut_range)


def estimate_l(cut):
    if cut > 3.0:
        sig = data.query(f"is_signal == 1 and {bdt_col} <= 3.0")["weight"].sum()
        bkg = data.query(f"is_signal == 0 and {bdt_col} <= 3.0")["weight"].sum()
        l1 = L(sig, bkg)
        sig = data.query(f"{bdt_col} > 3.0 and is_signal == 1 and {bdt_col} <= {cut}")["weight"].sum()
        bkg = data.query(f"{bdt_col} > 3.0 and is_signal == 0 and {bdt_col} <= {cut}")["weight"].sum()
        l2 = L(sig, bkg)
        sig = data.query(f"{bdt_col} > 3.0 and is_signal == 1 and {bdt_col} > {cut}")["weight"].sum()
        bkg = data.query(f"{bdt_col} > 3.0 and is_signal == 0 and {bdt_col} > {cut}")["weight"].sum()
        l3 = L(sig, bkg)
    else:
        sig = data.query(f"is_signal == 1 and {bdt_col} > 3.0")["weight"].sum()
        bkg = data.query(f"is_signal == 0 and {bdt_col} > 3.0")["weight"].sum()
        l1 = L(sig, bkg)
        sig = data.query(f"{bdt_col} <= 3.0 and is_signal == 1 and {bdt_col} <= {cut}")["weight"].sum()
        bkg = data.query(f"{bdt_col} <= 3.0 and is_signal == 0 and {bdt_col} <= {cut}")["weight"].sum()
        l2 = L(sig, bkg)
        sig = data.query(f"{bdt_col} <= 3.0 and is_signal == 1 and {bdt_col} > {cut}")["weight"].sum()
        bkg = data.query(f"{bdt_col} <= 3.0 and is_signal == 0 and {bdt_col} > {cut}")["weight"].sum()
        l3 = L(sig, bkg)
    return (l1 ** 2 + l2 ** 2 + l3 ** 2) ** 0.5


for i, cut in enumerate(cut_range):
    l_range[i] = estimate_l(cut)

plt.plot(cut_range, l_range, label="second split after split at 3.0")
plt.legend()
plt.xlabel("OffZ BDT cut")
plt.ylabel("Significance estimate L")

if nonh_only:
    plt.savefig("plots/offz_bdt_significance_scan_nonh.pdf")
    plt.savefig("plots/offz_bdt_significance_scan_nonh.png", dpi=300)
else:
    plt.savefig("plots/offz_bdt_significance_scan.pdf")
    plt.savefig("plots/offz_bdt_significance_scan.png", dpi=300)
