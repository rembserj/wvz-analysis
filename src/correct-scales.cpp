#include <vector>
#include <iostream>
#include <unordered_map>
#include <stdexcept>

#include "TFile.h"
#include "TTree.h"

int main(int argc, char** argv) {

    std::string wvzDataPath = std::getenv("WVZ_DATA_PATH");
    TFile outFile((wvzDataPath + "/babies/WVZMVAAllYearSkim4RegionsV3_v0.1.15/correct_scales/" + argv[1]).c_str(), "RECREATE");
    TTree outTree("t", "All events");

    TFile inFile((wvzDataPath + "/babies/WVZMVAAllYearSkim4RegionsV3_v0.1.15/" + argv[1]).c_str(), "READ");
    TTree& tree = *dynamic_cast<TTree*>(inFile.Get("t"));

    double scale = 1.0f;
    outTree.Branch("scale", &scale, "scale/D");

    TString * cms4path = nullptr;

    tree.SetBranchStatus("*", 0);
    tree.SetBranchStatus("CMS4path", 1);

    tree.SetBranchAddress("CMS4path", &cms4path);
    
    int n = tree.GetEntries();

    std::cout << n << std::endl;

    for (int i = 0; i < n; ++i) {

        if(i % 100000 == 0) {
            std::cout << i << std::endl;
        }

        tree.GetEntry(i);
        scale = 1.0f;

        if(cms4path->Contains("HZJ_HToWWTo2L2Nu_ZTo2L_M125_13TeV_powheg_pythia8_RunIISummer16MiniAODv3-PUMoriond17_94X_mcRun2_asymptotic_v3-v2")) {
            scale = 9.9601593625;
        }
        if(cms4path->Contains("GluGluHToZZTo4L_M125_13TeV_powheg2_JHUgenV6_pythia8_RunIISummer16MiniAODv3-PUMoriond17_94X_mcRun2_asymptotic_v3-v1")) {
            scale = 2.9735355337;
        }

        outTree.Fill();
    }

    outFile.Write();

    return 0;
}
