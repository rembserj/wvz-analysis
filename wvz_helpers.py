import uproot
import os
import glob
import numpy as np
import pandas as pd
import uproot_methods
import pickle

from geeksw.utils.data_loader_tools import make_data_loader, TreeWrapper


def get_weights(df):
    lepsf = df["looper_lepsf"]
    btagsf = df["looper_btagsf"]
    return df["looper_eventweight"] * lepsf * btagsf


def is_ChannelEMu(df):
    lep3_id = df["looper_lep3Id"]
    lep4_id = df["looper_lep4Id"]
    return np.logical_and(lep3_id * lep4_id == -143, df["nb"] == 0)


def is_ChannelEMuHighMT(df):
    return np.logical_and.reduce([is_ChannelEMu(df), df["looper_lep3MT"] > 40.0, df["looper_lep4MT"] > 20.0])


def is_ChannelBTagEMu(df):
    lep3_id = df["looper_lep3Id"]
    lep4_id = df["looper_lep4Id"]
    return np.logical_and(lep3_id * lep4_id == -143, df["nb"] > 0)


def is_ChannelOnZ(df):
    z_mass = 91.1876
    lep3_id = df["looper_lep3Id"]
    lep4_id = df["looper_lep4Id"]
    return np.logical_and(np.abs(df["looper_MllN"] - z_mass) < 10.0, lep3_id + lep4_id == 0)


def is_ChannelOffZ(df):
    z_mass = 91.1876
    lep3_id = df["looper_lep3Id"]
    lep4_id = df["looper_lep4Id"]
    return np.logical_and(np.abs(df["looper_MllN"] - z_mass) >= 10.0, lep3_id + lep4_id == 0)


def fetch_skims():
    skim_files = glob.glob("/home/jonas/PhD/VVV/data/babies/WVZMVAAllYearSkim4RegionsV3_v0.1.15/*.root")
    # skim_files = glob.glob("/home/llr/cms/rembser/scratch/babies/WVZMVAAllYearSkim4RegionsV3_v0.1.15/*.root")
    skims = {file_name.split("/")[-1][:-5]: file_name for file_name in skim_files}
    return skims


sample_combinations = {
    "www": [],  # there are no four-lepton events for WWW in the simulation so we won't bother
    "nonh_www": [],
    "wh_www": [],
    "wwz": ["nonh_wwz", "zh_wwz"],
    "wzz": ["nonh_wzz", "wh_wzz"],
    "zzz": ["nonh_zzz", "zh_zzz"],
    "nonh_sig": ["nonh_wwz", "nonh_wzz", "nonh_zzz"],
    "vh_sig": ["zh_wwz", "wh_wzz", "zh_zzz"],
    "sig": ["wwz", "wzz", "zzz"],
    "triother": ["zz", "twz", "rare"],
    "rarevvv": ["rare", "zh_wzz", "nonh_wzz", "zh_zzz", "nonh_zzz"],
    "dyttbar": ["dy", "ttbar"],
    "othernoh": ["rare", "dy", "ttbar"],
    "othervvv": ["higgs", "rare", "dy", "ttbar", "zh_wzz", "nonh_wzz", "zh_zzz", "nonh_zzz"],
    "other": ["higgs", "rare", "dy", "ttbar"],
}


skims = fetch_skims()


def load_dataframe(sample, exclude_training=False, cms4path=False, entrystop=None):

    branches = [
        "run",
        "lumi",
        "evt",
        "looper_eventweight",
        "looper_lepsf",
        "looper_btagsf",
        "looper_jet1Pt",
        "looper_MllN",
        "met_pt",
        "met_orig_pt",
        "looper_ZPt",
        "looper_minDRJetToLep3",
        "looper_minDRJetToLep4",
        "looper_lep1Pt",
        "looper_lep2Pt",
        "looper_lep3Pt",
        "looper_lep4Pt",
        "looper_pt_zeta",
        "looper_pt_zeta_vis",
        "looper_lep3MT",
        "looper_lep4MT",
        "looper_lep3dZ",
        "looper_lep4dZ",
        "looper_lep3Id",
        "looper_lep4Id",
        "looper_mt2",
        "looper_scalarsum_pt_4l",
        "looper_m_4l",
        "looper_vecsum_pt_4l",
        "nb",
        "nj",
        "nGenTau",
    ]

    if cms4path:
        branches += ["CMS4path"]

    load_tree = make_data_loader(branches, producers={})

    tree = TreeWrapper(uproot.open(skims[sample])["t"], n_max_events=entrystop)

    # data = tree.arrays(branches)
    df = pd.DataFrame(load_tree(tree))

    df["evt"] = np.array(df["evt"], dtype=np.int)

    df["is_ChannelEMu"] = is_ChannelEMu(df)
    df["is_ChannelEMuHighMT"] = is_ChannelEMuHighMT(df)
    df["is_ChannelBTagEMu"] = is_ChannelBTagEMu(df)
    df["is_ChannelOnZ"] = is_ChannelOnZ(df)
    df["is_ChannelOffZ"] = is_ChannelOffZ(df)

    df["n_gen_with_tau_mother"] = (np.abs(tree["lep_mc_motherid"]) == 15).sum()

    if sample == "data":
        df["weight"] = get_weights(df)
    else:
        correct_scales = uproot.open(skims[sample].replace(sample, "correct_scales/" + sample))["t"].array(
            "scale", entrystop=entrystop
        )
        df["weight"] = get_weights(df) * correct_scales

    # BDT scores
    score_dict = {
        "EMu_ttz_score_wwz": "scores/wwz/emu_ttz",
        "EMu_zz_score_wwz": "scores/wwz/emu_zz",
        "OffZ_zz_score_wwz": "scores/wwz/offz_zz",
        "EMu_ttz_score_nonh_wwz": "scores/nonh_wwz/emu_ttz",
        "EMu_zz_score_nonh_wwz": "scores/nonh_wwz/emu_zz",
        "OffZ_zz_score_nonh_wwz": "scores/nonh_wwz/offz_zz",
    }

    if False:
        for score_column, path in score_dict.items():
            scores = pd.read_hdf(path + "_sample_" + sample + ".hdf")[score_column].values
            if not entrystop is None:
                scores = scores[:entrystop]
            df[scores.columns] = scores
    else:
        scores = pd.read_hdf("bdt_scores_wwz" + "/" + sample + ".hdf")
        if not entrystop is None:
            scores = scores[:entrystop]
        df[scores.columns] = scores

    if False:
        if os.path.isfile("models/EMu2DBinning.pickle"):
            with open("models/EMu2DBinning.pickle", "rb") as handle:
                box_model = pickle.load(handle)
            df["EMu_bdt_bin"] = box_model.apply(df[["EMu_zz_score_wwz", "EMu_ttz_score_wwz"]].values)

            OffZ_bdt_bin = (df["OffZ_zz_score_wwz"].values > 0.0) * 6 + (df["OffZ_zz_score_wwz"].values > 3.0) - 1

            df["bdt_bin"] = (
                df.eval("(is_ChannelEMu or is_ChannelBTagEMu) * EMu_bdt_bin")
                + df.eval("is_ChannelOnZ or is_ChannelOffZ") * OffZ_bdt_bin
            )

        if os.path.isfile("models/EMu2DBinning_nonh.pickle"):
            with open("models/EMu2DBinning_nonh.pickle", "rb") as handle:
                box_model = pickle.load(handle)

            df["EMu_nonh_bdt_bin"] = box_model.apply(df[["EMu_zz_score_nonh_wwz", "EMu_ttz_score_nonh_wwz"]].values)
            OffZ_nonh_bdt_bin = (
                (df["OffZ_zz_score_nonh_wwz"].values > 0.0) * 6 + (df["OffZ_zz_score_nonh_wwz"].values > 3.5) - 1
            )

            df["nonh_bdt_bin"] = (
                df.eval("(is_ChannelEMu or is_ChannelBTagEMu) * EMu_nonh_bdt_bin")
                + df.eval("is_ChannelOnZ or is_ChannelOffZ") * OffZ_nonh_bdt_bin
            )

    df["used_for_EMu_training"] = False
    if sample == "ttz":
        df.loc[df["is_ChannelEMu"], "used_for_EMu_training"] = df.loc[df["is_ChannelEMu"], "evt"] % 3 != 0
        df.loc[df["is_ChannelBTagEMu"], "used_for_EMu_training"] = df.loc[df["is_ChannelBTagEMu"], "evt"] % 3 != 0
    if sample in ["nonh_wwz", "zh_wwz", "zz", "wwz"]:
        df.loc[df["is_ChannelEMu"], "used_for_EMu_training"] = df.loc[df["is_ChannelEMu"], "evt"] % 3 != 0

    df["used_for_OffZ_training"] = False
    if sample in ["wwz", "nonh_wwz", "zh_wwz", "zz"]:
        df["used_for_OffZ_training"] = df.eval("evt % 3 != 0 and is_ChannelOffZ")

    if exclude_training:

        if sample == "ttz":
            df.loc[np.logical_and(df["is_ChannelEMu"], ~df["used_for_EMu_training"]), "weight"] *= 3.0
            df.loc[np.logical_and(df["is_ChannelBTagEMu"], ~df["used_for_EMu_training"]), "weight"] *= 3.0
        if sample in ["nonh_wwz", "zh_wwz", "zz", "wwz"]:
            df.loc[np.logical_and(df["is_ChannelEMu"], ~df["used_for_EMu_training"]), "weight"] *= 3.0
            df.loc[np.logical_and(df["is_ChannelOffZ"], ~df["used_for_OffZ_training"]), "weight"] *= 3.0
            df.loc[np.logical_and(df["is_ChannelBTagEMu"], ~df["used_for_EMu_training"]), "weight"] *= 3.0

        return df.query("not used_for_OffZ_training and not used_for_EMu_training").copy()

    return df


def load_dataframes(samples, concat=False, exclude_training=False, entrystop=None):

    dfs = {}
    for s in samples:
        if s in sample_combinations:
            dfs[s] = load_dataframes(
                sample_combinations[s], concat=True, exclude_training=exclude_training, entrystop=entrystop
            )
        else:
            dfs[s] = load_dataframe(s, exclude_training=exclude_training, entrystop=entrystop)
        dfs[s]["sample"] = s
    if concat:
        if dfs == {}:
            return pd.DataFrame()
        return pd.concat(dfs.values(), ignore_index=True)
    else:
        return dfs
