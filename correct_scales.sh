#!/bin/bash

cd $WVZ_DATA_PATH/babies/WVZMVAAllYearSkim4RegionsV3_v0.1.15/

mkdir -p correct_scales/
for file in *.root
do
  echo $file
  /home/jonas/PhD/VVV/wvz-analysis/build/bin/correct-scales "$file"
done
